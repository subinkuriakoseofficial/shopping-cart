<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Add Product') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('storeProduct') }}" enctype="multipart/form-data"> 
                        @csrf

                        <div>
                            <label for="name">Product Name</label>
                            <input id="name" class="block mt-1 w-full" type="text" name="name" value="{{ old('name') }}" autofocus />
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div>
                            <label for="sku">SKU</label>
                            <input id="sku" class="block mt-1 w-full" type="text" name="sku" value="{{ old('sku') }}" />
                            @error('sku')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div>
                            <label for="image">Image</label>
                            <input id="image" class="block mt-1 w-full" type="file" name="image" />
                            @error('image')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div>
                            <label for="price">Price</label>
                            <input id="price" class="block mt-1 w-full" type="text" name="price" value="{{ old('price') }}" />
                            @error('price')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div>
                            <label for="stock">Stock</label>
                            <input id="stock" class="block mt-1 w-full" type="text" name="stock" value="{{ old('stock') }}" />
                            @error('stock')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <x-button class="ml-4" style="margin-top:5px; float:right; margin-bottom:10px;">
                            {{ __('Submit') }}
                        </x-button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
