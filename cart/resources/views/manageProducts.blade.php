<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    @if(Session::has('message'))
                        <p class="alert">{{ Session::get('message') }}</p>
                    @endif

                    <table>
                        <tr>
                            <td>Name</td>
                            <td>SKU</td>
                            <td>Thumbnail</td>
                            <td>Price</td>
                            <td>Stock</td>
                            <td>Actions</td>
                        </tr>
                        @foreach($products as $row)
                            @php $id = Crypt::encryptString($row->id); @endphp
                            <tr>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->sku }}</td>
                                <td><img src="{{ asset('/storage/' . $row->image) }}" width="100px"></td>
                                <td>{{ $row->price }}</td>
                                <td>{{ $row->stock }}</td>
                                <td><a href="{{ route('editProduct', ['id' => $id]) }}">Edit</a> &nbsp; <a href="{{ route('deleteProduct', ['id' => $id]) }}" onclick="return confirm('Delete product ?')">Delete</a>
                            </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
