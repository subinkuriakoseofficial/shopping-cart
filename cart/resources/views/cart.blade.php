<x-guest-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Manage Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    @if(Session::has('message'))
                        <p class="alert">{{ Session::get('message') }}</p>
                    @endif

                    <div class="col-md-6">
                    @csrf

                    <table>
                        <tr>
                            <td>Name</td>
                            <td>Price</td>
                            <td>Quantity</td>
                        </tr>
                        @foreach($products as $row)
                            <tr>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->price }}</td>
                                <td><input type="number" class="quantity" data-id="{{ $row->id }}" name="quantity" min="0" value="0" /></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="5">
                                <x-button id="addToCart" data-url="{{ route('addtocart') }}" class="ml-4" style="margin-top:5px; float:right; margin-bottom:10px;">
                                    {{ __('Add to Cart') }}
                                </x-button>
                            </td>
                        </tr>
                    </table>

                    </div>
                    <div class="col-md-6">
                        <table>
                            <th colspan="5" style="text-align:center;">Cart</th>
                            <tr>
                                <th>Item Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Action</th>
                            </tr>
                            @foreach($cart as $idx => $row)
                                <tr>
                                    <td>{{ $row['name'] }}</td>
                                    <td>{{ $row['price'] }}</td>
                                    <td>{{ $row['quantity'] }}</td>
                                    <td>{{ $row['total'] }}</td>
                                    <td><a href="{{ route('deleteCartItem', ['id' => $idx]) }}" onclick="return confirm('You want to delete the item from cart ?')" title="delete">X</a></td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="3"> Grand Total: </td>
                                <td colspan="2">{{ $grandTotal }}</td>
                            </tr>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-guest-layout>
