$(function(){
$("#addToCart").click(function(){
    var dataArray = [];
    $(".quantity").each(function(){
        var newData = {
            'prodId' : $(this).data("id"),
            'quantity' : $(this).val(),
        };
        dataArray.push(newData);
    });
    console.log(dataArray);

    var url = $(this).data('url');
    console.log(url);
    $.ajax({
        url: url,
        type: "POST",
        data: {
            _token: $('[name="csrf-token"]').attr('content'),
            cart: dataArray
        },
        success: function(data) {
            if(data == true) {
                location.reload();
            }
        },
        error: function(data) {
            alert('Error occured, Please try again later');
        }
    })

});

});