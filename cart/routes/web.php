<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::controller(CartController::class)->group(function () {
    Route::get('/cart', 'index')->name('cart');
    Route::post('/cart/add', 'store')->name('addtocart');
    Route::get('/cart/delete{id}', 'destroy')->name('deleteCartItem');
});

Route::prefix('admin')->middleware(['auth'])->controller(ProductController::class)->group(function () {

    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('/products', 'index')->name('products');

    Route::get('/products/add', 'create')->name('addProduct');

    Route::post('/products/add', 'store')->name('storeProduct');

    Route::get('/products/edit/{id}', 'edit')->name('editProduct');

    Route::post('/products/edit', 'update')->name('updateProduct');

    Route::get('/products/delete/{id}', 'destroy')->name('deleteProduct');

});


require __DIR__.'/auth.php';
