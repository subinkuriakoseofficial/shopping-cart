<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    public function getAllProducts() 
    {
        return Product::all();
    }

    public function get($id) 
    {
        return Product::find($id);
    }

    public function save($data)
    {
        return Product::create($data);
    }

    public function update($data)
    {
        return Product::where('id', $data['id'])->update($data);
    }

    public function delete($id)
    {
        $product = Product::find($id);
        return ($product->delete()) ? $product : false;
    }

    public function getProducts($idArr) 
    {
        return Product::whereIn('id', $idArr)->get();
    }



}