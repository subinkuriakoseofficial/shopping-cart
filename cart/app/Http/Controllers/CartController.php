<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;

use App\Repositories\ProductRepository;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Crypt;

class CartController extends Controller
{

    public function __construct()
    {
        $this->productRepo = new ProductRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = $this->productRepo->getAllProducts();

        $cart = [];
        $cart = $request->session()->get('cart', $cart);
        
        $grandTotal = (!empty($cart)) ? array_sum(array_column($cart, 'total')) : 0; 

        return view('cart', compact('products', 'cart', 'grandTotal'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->cart;

        $quantity = [];
        $prodId = [];
        foreach($data as $row) {
            $quantity[$row['prodId']] = $row['quantity'];
            $prodId[] = $row['prodId'];
        }
        // return $quantity;

        $products = $this->productRepo->getProducts($prodId);
        $cart = [];
        foreach($products as $row) {
            $cartQuantity = $quantity[$row->id];
            if($cartQuantity > 0) {
                $total = $cartQuantity * $row->price;
                $cart[] = array(
                    'prodId' => $row->id,
                    'name' => $row->name,
                    'price' => $row->price,
                    'quantity' => $cartQuantity,
                    'total' => $total,
                );
            }
        }

        $request->session()->put('cart', $cart);
        return true;
        
    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $cart = [];
        $cart = $request->session()->get('cart', $cart);
        unset($cart[$id]);
        $request->session()->put('cart', $cart);

        return redirect('cart')->with('message', 'Item removed successfully');
    }
}
