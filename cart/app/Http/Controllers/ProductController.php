<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;

use App\Repositories\ProductRepository;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Crypt;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->productRepo = new ProductRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->productRepo->getAllProducts();
        return view('manageProducts', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('addProduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $data = $request->all();
        $image = Storage::disk('public')->putFile('product', $request->file('image'));
        $data['image'] = $image;
        $save = $this->productRepo->save($data);
        if($save) {
            return redirect('admin/products')->with('message', 'Product added successfully');
        }
        else {
            return back()->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = Crypt::decryptString($id);
        $product = $this->productRepo->get($id);
        return view('editProduct', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request)
    {
        $data = $request->all();
        if($request->hasFile('image')) {
            $image = Storage::disk('public')->putFile('product', $request->file('image'));
            $xImage = $data['x-image'];
        }
        else {
            $image = $data['x-image'];
        }
        
        $data['image'] = $image;
        unset($data['_token'], $data['x-image']);

        $save = $this->productRepo->update($data);
        if($save) {
            if(!empty($xImage)) {
                Storage::disk('public')->delete($xImage);
            }
            return redirect('admin/products')->with('message', 'Product updated successfully');
        }
        else {
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = Crypt::decryptString($id);
        $product = $this->productRepo->delete($id);
        if($product) {
            Storage::disk('public')->delete($product->image);
            return redirect('admin/products')->with('message', 'Product deleted successfully');
        }
        else {
            return redirect('admin/products')->with('message', 'Unable to delete product');
        }
    }
}
